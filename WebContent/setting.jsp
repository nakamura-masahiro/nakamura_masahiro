<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
                <a href="management">ユーザー管理</a>
            </div>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <form action="setting" method="post"><br />
                <label for="account">アカウント</label>
                <c:if test="${not empty errorEditUser}">
                	<input type="text" name="account" id="account" value="${errorEditUser.account}" /> <br />
                </c:if>
                <c:if test="${empty errorEditUser}">
                	<input type="text" name="account" id="account" value="${editUser.account}" /> <br />
                </c:if>

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

				<label for="password">確認用パスワード</label>
                <input name="checkPassword" type="password" id="checkPassword" /> <br />

				<label for="name">名前</label>
				<c:if test="${not empty errorEditUser}">
                	<input type="text" name="name" id="name" value="${errorEditUser.name}" /> <br />
                </c:if>
                <c:if test="${empty errorEditUser}">
                	<input type="text" name="name" id="name" value="${editUser.name}" /> <br />
                </c:if>

                <label for="branchId">支社</label>
				<c:if test="${not empty errorEditUser}">
	                <select name="branchId">
	                	<c:forEach items="${branches}" var="branch">
	                		<option value="${branch.id}" <c:if test="${branch.id==errorEditUser.branchId}">selected
	                		</c:if>>${branch.name}</option>
	                	</c:forEach>
	                </select> <br />
                </c:if>
                <c:if test="${empty errorEditUser}">
                	<c:if test="${loginUser.id!=editUser.id}">
	                	<select name="branchId">
		                	<c:forEach items="${branches}" var="branch">
		                		<option value="${branch.id}" <c:if test="${branch.id==editUser.branchId}">selected
		                		</c:if>>${branch.name}</option>
		                	</c:forEach>
		                </select> <br />
	                </c:if>
	                <c:if test="${loginUser.id==editUser.id}">
	                	<c:forEach items="${branches}" var="branch">
	                		<c:if test="${branch.id==editUser.branchId}">
	                			<c:out value="${branch.name}" /> <br />
                			</c:if>
                		</c:forEach>
	                </c:if>
                </c:if>

                <label for="departmentId">部署</label>
                <c:if test="${not empty errorEditUser}">
	                <select name="departmentId">
	                	<c:forEach items="${departments}" var="department">
	                		<option value="${department.id}" <c:if test="${department.id==errorEditUser.departmentId}">selected
	                		</c:if>>${department.name}</option>
	                	</c:forEach>
	                </select> <br />
                </c:if>
                <c:if test="${empty errorEditUser}">
                	<c:if test="${loginUser.id!=editUser.id}">
	                	<select name="departmentId">
		                	<c:forEach items="${departments}" var="department">
		                		<option value="${department.id}" <c:if test="${department.id==editUser.departmentId}">selected
		                		</c:if>>${department.name}</option>
		                	</c:forEach>
		                </select> <br />
	                </c:if>
	                <c:if test="${loginUser.id==editUser.id}">
	                	<c:forEach items="${departments}" var="department">
	                		<c:if test="${department.id==editUser.departmentId}">
	                			<c:out value="${department.name}" /> <br />
                			</c:if>
                		</c:forEach>
	                </c:if>
                </c:if>

				<c:if test="${not empty errorEditUser}">
					<input type="hidden" name="editUserId" value="${errorEditUser.id}">
				</c:if>
				<c:if test="${empty errorEditUser}">
					<input type="hidden" name="editUserId" value="${editUser.id}">
				</c:if>
                <input type="submit" value="更新" /> <br />
            </form>

            <div class="copyright">Copyright(c)Nakamura Masahiro</div>
        </div>
	</body>
</html>