<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
            <div class="header">
                <a href="message">新規投稿</a>
                <c:if test="${loginUser.branchId==1 && loginUser.departmentId==1}">
                	<a href="management">ユーザー管理</a>
                </c:if>
                <a href="logout">ログアウト</a>
            </div>

			<c:if test="${ not empty errorMessage }">
			    <div class="errorMessage">
			        <ul>
		                <li><c:out value="${errorMessage}" />
			        </ul>
			    </div>
			    <c:remove var="errorMessage" scope="session" />
			</c:if>

            <div class="narrow-down">
            	<form action="./" method="get">
            		<label for="date">日付</label>
                	<input type="date" name="start" id="start" value="${start}" /> ～
                	<input type="date" name="end" id="end" value="${end}" /> <br />

                	<label for="category">カテゴリ</label>
                	<input type="text" name="category" id="category" value="${category}" />
                	<input type="submit" value="絞り込み">
            	</form>
            </div>

            <c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="errorMessage">
			                <li><c:out value="${errorMessage}" />
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session" />
			</c:if>

            <div class="messages">
			    <c:forEach items="${messages}" var="message">
			    <div class="message-comment">
			        <div class="message">
		        		<div class="name"><c:out value="${message.name}" /></div>
						<div class="title"><c:out value="${message.title}" /></div>
			            <div class="category"><c:out value="${message.category}" /></div>
			            <div class="text">
			            	<c:forEach items="${message.splitedText}" var="s">
			            		<div><c:out value="${s}" /></div>
			            	</c:forEach>
			            </div>
			            <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			        </div>
			        <c:if test="${message.userId==loginUser.id}">
						<div class="message-delete">
							<form action="deleteMessage" method="post" onsubmit="return checkmessage()">
								<input type="hidden" name="messageId" value="${message.id}">
								<input type="submit" value="削除">
							</form>
							<script type="text/javascript">
								function checkmessage() {
									return confirm("投稿を削除しますか？");
								}
							</script>
						</div>
					</c:if>

					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id==comment.messageId}">
								<div class="comment">
									<div class="name"><c:out value="${comment.name}" /></div>
									<div class="text">
										<c:forEach items="${comment.splitedText}" var="s">
											<div><c:out value="${s}" /></div>
										</c:forEach>
									</div>
									<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								</div>
								<c:if test="${comment.userId==loginUser.id}">
									<div class="comment-delete">
										<form action="deleteComment" method="post" onsubmit="return checkcomment()">
											<input type="hidden" name="commentId" value="${comment.id}">
											<input type="submit" value="削除">
										</form>
										<script type="text/javascript">
											function checkcomment() {
												return confirm("コメントを削除しますか？");
											}
										</script>
									</div>
								</c:if>
							</c:if>
						</c:forEach>
					</div>

			        <div class="form-area">
				        <form action="comment" method="post">
				            コメント入力欄(500文字以内)<br />
				            <textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
				            <br />
				            <input type="hidden" name="messageId" value="${message.id}">
				            <input type="submit" value="コメント投稿">
				        </form>
					</div>
				</div>
			    </c:forEach>
			</div>

            <div class="copyright"> Copyright(c)Nakamura Masahiro</div>
        </div>
	</body>
</html>