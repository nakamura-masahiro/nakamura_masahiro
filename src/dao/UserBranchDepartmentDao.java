package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    users.id AS id, ");
            sql.append("    users.account AS account, ");
            sql.append("    users.password AS password, ");
            sql.append("    users.name AS name, ");
            sql.append("    users.branch_id AS branch_id, ");
            sql.append("    branches.name AS branch_name, ");
            sql.append("    users.department_id AS department_id, ");
            sql.append("    departments.name AS department_name, ");
            sql.append("    users.is_stopped AS is_stopped, ");
            sql.append("    users.created_date AS created_date, ");
            sql.append("    users.updated_date AS updated_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");
            sql.append("ORDER BY created_date DESC; ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<UserBranchDepartment> userInformations = toUserBranchDepartments(rs);
            return userInformations;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserBranchDepartment> toUserBranchDepartments(ResultSet rs) throws SQLException {

        List<UserBranchDepartment> userInformations = new ArrayList<UserBranchDepartment>();
        try {
            while (rs.next()) {
            	UserBranchDepartment userInformation = new UserBranchDepartment();
            	userInformation.setId(rs.getInt("id"));
            	userInformation.setAccount(rs.getString("account"));
            	userInformation.setPassword(rs.getString("password"));
            	userInformation.setName(rs.getString("name"));
            	userInformation.setBranchId(rs.getInt("branch_id"));
            	userInformation.setBranchName(rs.getString("branch_name"));
            	userInformation.setDepartmentId(rs.getInt("department_id"));
            	userInformation.setDepartmentName(rs.getString("department_name"));
            	userInformation.setIsStopped(rs.getInt("is_stopped"));
            	userInformation.setCreatedDate(rs.getTimestamp("created_date"));
            	userInformation.setUpdatedDate(rs.getTimestamp("updated_date"));

                userInformations.add(userInformation);
            }
            return userInformations;
        } finally {
            close(rs);
        }
    }
}
