package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category = request.getParameter("category");

		request.setAttribute("start", start);
		request.setAttribute("end", end);
		request.setAttribute("category", category);

		//メッセージ取得
		List<UserMessage> messages = new MessageService().select(start, end, category);

		/*UserMessage message = messages.get(0);
		String[] splitText = message.getSplitedText();
		System.out.println("出力します");
		for (String oneLine : splitText) {
			System.out.println(oneLine);
		}*/

		//メッセージをリクエストにセット
        request.setAttribute("messages", messages);

        //コメント取得
  		List<UserComment> comments = new CommentService().select();
  		//コメントをリクエストにセット
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
}
