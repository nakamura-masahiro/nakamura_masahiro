package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	        throws IOException, ServletException{

		int userInformationId = Integer.parseInt(request.getParameter("userInformationId"));
		int userInformationIsStopped = Integer.parseInt(request.getParameter("userInformationIsStopped"));

		int num = 0;
		if(userInformationIsStopped == 0) {
			 num = 1;
		} else {
			 num = 0;
		}

		new UserService().stopUpdate(userInformationId, num);

		response.sendRedirect("./management");
	}
}
