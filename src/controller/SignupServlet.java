package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		//支社取得
		List<Branch> branches = new BranchService().select();
		//支社をリクエストにセット
        request.setAttribute("branches", branches);

        //部署取得
		List<Department> departments = new DepartmentService().select();
		//部署をリクエストにセット
	    request.setAttribute("departments", departments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	        throws IOException, ServletException{

		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);
		String checkPassword = request.getParameter("checkPassword");
		if (!isValid(user, errorMessages, checkPassword)) {
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", errorMessages);
            String errorAccount = user.getAccount();
            String errorName = user.getName();
            int errorBranchId = user.getBranchId();
            int errorDepartmentId = user.getDepartmentId();
            session.setAttribute("errorAccount", errorAccount);
            session.setAttribute("errorName", errorName);
            session.setAttribute("errorBranchId", errorBranchId);
            session.setAttribute("errorDepartmentId", errorDepartmentId);
            response.sendRedirect("./signup");
            return;
        }
		new UserService().insert(user);
        response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages, String checkPassword) {

    	String account = user.getAccount();
        String password = user.getPassword();
        String name = user.getName();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        User checkUser = new UserService().select(account);

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウントを入力してください");
        } else if (account.length() < 6) {
        	errorMessages.add("アカウントは6文字以上で入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウントは20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        } else if (! password.equals(checkPassword)) {
        	errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
        }

        if ((! StringUtils.isEmpty(password)) && (password.length() < 6)) {
        	errorMessages.add("パスワードは6文字以上で入力してください");
        } else if (20 < password.length()) {
            errorMessages.add("パスワードは20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(name)) {
            errorMessages.add("名前を入力してください");
        } else if (10 < name.length()) {
        	errorMessages.add("名前は10文字以下で入力してください");
        }

        if (checkUser != null) {
        	errorMessages.add("アカウントが重複しています");
        }

        if ((branchId == 1 && departmentId == 3) ||
        		(branchId == 1 && departmentId == 4) ||
        		(branchId == 2 && departmentId == 1) ||
        		(branchId == 2 && departmentId == 2) ||
        		(branchId == 3 && departmentId == 1) ||
        		(branchId == 3 && departmentId == 2) ||
        		(branchId == 4 && departmentId == 1) ||
        		(branchId == 4 && departmentId == 2)) {
        	errorMessages.add("支社と部署の組み合わせが不正です");
        }

        if (errorMessages.size() == 0) {
            return true;
        }
        return false;
    }
}
